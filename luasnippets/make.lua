-- local vars see https://github.com/L3MON4D3/LuaSnip/blob/ce0a05ab4e2839e1c48d072c5236cce846a387bc/lua/luasnip/default_config.lua#L20-L98

return {
  s(
    "target",
    fmt(
      [[
        .PHONY: {}
        {}: ## {}
        	{}
        ]],
      {
        i(1, "target_name"),
        rep(1),
        i(2, "help description"),
        i(0),
      }
    )
  ),
}
