return {
  { "akinsho/bufferline.nvim", enabled = false },
  -- {
  --   "lukas-reineke/headlines.nvim",
  --   config = function(_, opts)
  --     -- set highlights for "headlines" plugin -> adopt zenbones values
  --     vim.cmd([[highlight Headline guibg=#1C1917 guifg=#E4EDF3 gui=bold]])
  --     vim.cmd([[highlight CodeBlock guibg=#2C2927]])
  --
  --     require("headlines").setup(opts)
  --   end,
  -- },
  {
    "folke/zen-mode.nvim",
    opts = {
      window = {
        backdrop = 0.95, -- shade the backdrop of the Zen window. Set to 1 to keep the same as Normal
        -- height and width can be:
        -- * an absolute number of cells when > 1
        -- * a percentage of the width / height of the editor when <= 1
        -- * a function that returns the width or the height
        width = 100, -- width of the Zen window
        height = 1, -- height of the Zen window
        options = {
          number = false, -- disable number column
          -- signcolumn = "no", -- disable signcolumn
          -- relativenumber = false, -- disable relative numbers
          -- cursorline = false, -- disable cursorline
          -- cursorcolumn = false, -- disable cursor column
          -- foldcolumn = "0", -- disable fold column
          -- list = false, -- disable whitespace characters
        },
      },
      plugins = {
        options = {
          enabled = true,
          ruler = false, -- disables the ruler text in the cmd line area
          showcmd = false, -- disables the command in the last line of the screen
          -- you may turn on/off statusline in zen mode by setting 'laststatus'
          -- statusline will be shown only if 'laststatus' == 3
          laststatus = 0, -- turn off the statusline in zen mode
        },
        kitty = {
          enabled = true,
          font = "+4", -- font size increment
        },
      },
    },
    keys = {
      { "<leader>uz", "<cmd>ZenMode<cr>", desc = "Toogle Zen Mode", mode = "n", noremap = true },
    },
  },
}
